'use strict';

const async = require('async');
let log = require('@dosarrest/loopback-component-logger')('mod-admin-accounts/boot');
let cascadeDelete = require('loopback-cascade-delete-mixin/cascade-delete');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let registry = app.registry;
    let authOptions = app.get('auth');
    if (authOptions.enabled === true) {
      app.on('authenticationLoaded', () => {
        let AccountModel = me.app.registry.getModelByType('Account');
        let UserModel = me.app.registry.getModelByType('User');
        let RoleModel = me.app.registry.getModelByType('Role');
        let RoleMappingModel = me.app.registry.getModelByType('RoleMapping');
        UserModel.belongsTo(AccountModel, {foreignKey: 'accountId', as: 'account'});
        UserModel.nestRemoting('account');
        AccountModel.hasMany(UserModel, {as: 'users', foreignKey: 'accountId'});
        AccountModel.nestRemoting('users');
        cascadeDelete(AccountModel, {
          relations: ["users"],
          deepDelete: true
        });
        if (authOptions.setupDefaultUsers) {
          log.info('--- Setting up default accounts');
          async.series({
            owner: (cb) => {
              AccountModel.findOrCreate({
                where: {
                  name: 'Owner',
                },
              }, {
                name: 'Owner',
                description: 'Default Owner Account',
                parentId: '',
                type: '$ownerAccount',
                created: new Date(),
              }, cb);
            },
            reseller: (cb) => {
              AccountModel.findOrCreate({
                where: {
                  name: 'Reseller',
                },
              }, {
                name: 'Reseller',
                description: 'Default Reseller Account',
                parentId: '',
                type: '$resellerAccount',
                created: new Date(),
              }, cb);
            },
          }, (err, accounts) => {
            async.series({
              administrator: (cb) => {
                RoleModel.findOne({
                  where: {
                    name: 'Administrator',
                  },
                }, cb);
              },
              developer: (cb) => {
                RoleModel.findOne({
                  where: {
                    name: 'Developer',
                  },
                }, cb);
              },
              accountOwner: (cb) => {
                RoleModel.findOrCreate({
                  where: {
                    name: "Account Owner",
                  }
                }, {
                  name: "Account Owner",
                  description: "Owner of an account, only one per account",
                  created: new Date(),
                  modified: new Date()
                }, cb);
              },
              accountManager: (cb) => {
                RoleModel.findOrCreate({
                  where: {
                    name: "Account Manager",
                  }
                }, {
                  name: "Account Manager",
                  description: "Account Managers",
                  created: new Date(),
                  modified: new Date()
                }, cb);
              },
            }, (err, roles) => {
              if (err) log.error(err);
              log.info('New Roles Created:', roles);
              async.series({
                owner: (cb) => {
                  return UserModel.findOrCreate({
                    where: {
                      username: 'Owner',
                    },
                  }, {
                    'username': 'Owner',
                    'email': 'owner@email.com',
                    'emailVerified': true,
                    'password': 'test',
                    'accountId': accounts.owner[0].id,
                  }, cb);
                },
                reseller: (cb) => {
                  return UserModel.findOrCreate({
                    where: {
                      username: 'Reseller',
                    },
                  }, {
                    'username': 'Reseller',
                    'email': 'reseller@email.com',
                    'emailVerified': true,
                    'password': 'test',
                    'accountId': accounts.reseller[0].id,
                  }, cb);
                },
                administrator: (cb) => {
                  return UserModel.findOne({where: {username: 'Administrator'}})
                    .then((user) => {
                      user.updateAttribute('accountId', accounts.owner[0].id, cb);
                    }).catch(err => { cb(err); });
                },
                developer: (cb) => {
                  return UserModel.findOne({where: {username: 'Developer'}})
                    .then((user) => {
                      user.updateAttribute('accountId', accounts.owner[0].id, cb);
                    }).catch(err => { cb(err); });
                },
              }, (uErr, users) => {
                log.info('New Users Created:', users);
                async.series({
                  'owner-Administrator': (cb) => {
                    RoleMappingModel.findOrCreate({
                      principalType: RoleMappingModel.USER,
                      principalId: users.owner[0].id,
                      roleId: roles.administrator.id,
                    }, cb);
                  },
                  'owner-Developer': (cb) => {
                    RoleMappingModel.findOrCreate({
                      principalType: RoleMappingModel.USER,
                      principalId: users.owner[0].id,
                      roleId: roles.developer.id,
                    }, cb);
                  },
                  'owner-AccountOwner': (cb) => {
                    RoleMappingModel.findOrCreate({
                      principalType: RoleMappingModel.USER,
                      principalId: users.owner[0].id,
                      roleId: roles.accountOwner[0].id,
                    }, cb);
                  },
                }, (err, addedMappings) => {
                  log.info('Added Mappings:', addedMappings);
                  log.info('--- Setting up default accounts completed!');
                });
              });
            });
          });
        }
      });
    }

    let AdminNavigation = registry.getModelByType('AdminNavigation');
    AdminNavigation.find({where: {routeId: 'accounts', perspective: 'admin'}})
      .then(navItem => {
        if (navItem.length === 0) {
          AdminNavigation.create({
            text: 'mod-admin-accounts:ACCOUNTS',
            perspective: 'admin',
            iconCls: 'x-fa fa-id-card',
            // rowCls: 'nav-tree-badge nav-tree-badge-new',
            viewType: 'accounts',
            routeId: 'accounts',
            leaf: true,
          }).then(newNavItem => {
            log.info(newNavItem);
            done(null, true);
          }).catch(err => {
            log.error(err);
            done(null, false);
          });
        } else {
          done(null, true);
        }
      }).catch(err => {
        log.error(err);
        done(null, true);
      });
  }
}
module.exports = Boot;
