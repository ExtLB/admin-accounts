import { PersistedModel } from "loopback";
export declare class AccountInterface extends PersistedModel {
    id: string;
    name: string;
    description: string;
    type: string;
    created: Date;
}
export declare const AccountType: {
    REGULAR: string;
    OWNER: string;
    RESELLER: string;
};
export declare class Account extends PersistedModel {
}
export default function (Account: <Account>() => AccountInterface): void;
