import * as _ from 'lodash';
import {Promise} from 'es6-promise';
import {LoopBackApplication, PersistedModel, Role, User} from "loopback";
const LoopBackSocket = require('loopback-socket');
let log = require('@dosarrest/loopback-component-logger')('server/model/Account');

export class AccountInterface extends PersistedModel {
  id: string;
  name: string;
  description: string;
  type: string;
  created: Date;
}
export const AccountType = {
  REGULAR: '$regularAccount',
  OWNER: '$ownerAccount',
  RESELLER: '$resellerAccount',
};
export class Account extends PersistedModel {
}

class AccountModel {
  app: LoopBackApplication;
  model: <Account>() => AccountInterface;
  lbSocket: any;
  constructor(model: <Account>() => AccountInterface) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then(events => {
      return me.setupModel();
    }).then((methods) => {
      return me.setupMethods();
    }).catch(err => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        me.addResolvers();
        me.lbSocket = LoopBackSocket.get('app');
        log.info('Attached DISAccount Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).remoteMethod('getTree', {
        accepts: {arg: 'id', type: 'string', required: true},
        returns: {arg: 'results', type: 'object', root: true},
        http: {verb: 'get', path: '/tree/:id'},
      });
      (me.model as any).getTree = me.getTree;
      (me.model as any).remoteMethod('patchTree', {
        accepts: [
          {arg: 'id', type: 'string', required: true},
          {arg: 'name', type: 'string', required: false},
          {arg: 'description', type: 'string', required: false},
          {arg: 'parentId', type: 'string', required: false},
          {arg: 'type', type: 'string', required: false},
        ],
        returns: {arg: 'results', type: 'object', root: true},
        http: {verb: 'patch', path: '/tree/:id'},
      });
      (me.model as any).patchTree = me.patchTree;
      (me.model as any).remoteMethod('deleteTree', {
        accepts: {arg: 'id', type: 'string', required: true},
        returns: {arg: 'results', type: 'object', root: true},
        http: {verb: 'delete', path: '/tree/:id'},
      });
      (me.model as any).deleteTree = me.deleteTree;
      resolve(true);
    });
  }
  addResolvers() {
    let me = this;
    let app = me.app;
    let Role = (app as any).registry.getModelByType('Role');
    let User = (app as any).registry.getModelByType('User');
    let Account = (app as any).registry.getModelByType('Account');
    Role.registerResolver(AccountType.OWNER, function(role: Role, context: any, cb: (err: any, permitted?: boolean) => any) {
      if (context.modelName === 'Account') {
        return process.nextTick(() => cb(null, false));
      }
      let userId = context.accessToken.userId;
      if (!userId) {
        return process.nextTick(() => cb(null, false));
      }
      User.findById(userId, {
        include: ['account'],
      }).then((user: User) => {
        let account = (user as any).account();
        switch (account.type) {
          case AccountType.OWNER:
            cb(null, true);
            break;
          default:
            cb(null, false);
        }
      }).catch((err: any) => {
        log.error(err);
        cb(err, false);
      });
    });
    Role.registerResolver(AccountType.RESELLER, function(role: Role, context: any, cb: (err: any, permitted?: boolean) => any) {
      if (context.modelName === 'Account') {
        return process.nextTick(() => cb(null, false));
      }
      let userId = context.accessToken.userId;
      if (!userId) {
        return process.nextTick(() => cb(null, false));
      }
      User.findById(userId, {
        include: ['account'],
      }).then((user: User) => {
        let account = (user as any).account();
        switch (account.type) {
          case AccountType.RESELLER:
            cb(null, true);
            break;
          default:
            cb(null, false);
        }
      }).catch((err: any) => {
        log.error(err);
        cb(err, false);
      });
    });
  }
  getTree(id: string, next: Function): void {
    (this as any).find({where: {parentId: (id === 'root'?"":id)}, counts: ['users']}, (err: Error, results: Array<any>) => {
      _.each(results, (result, id) => {
        results[id].iconCls = 'x-fa fa-users';
        if (result.type !== AccountType.RESELLER) {
          results[id].leaf = true;
          results[id].iconCls = 'x-fa fa-user';
        }
      });
      next(err, {children: results, total: results.length, success: true});
    });
  }
  patchTree(id: string, name: string, description: string, parentId: string, type: string, next: Function): void {
    (this as any).findById(id, (err: Error, account: Account) => {
      if (err) {
        next(err);
      } else {
        let changes: any = {};
        if (name) {changes.name = name}
        if (description) {changes.description = description}
        if (type) {changes.type = type}
        if (parentId) {changes.parentId = parentId}
        if (type === AccountType.RESELLER || type === AccountType.OWNER) {
          changes.parentId = '';
        }
        account.updateAttributes(changes, (updateErr: Error, result: Account) => {
          next(updateErr, result);
        });
      }
    });
  }
  deleteTree(id: string, next: Function): void {
    (this as any).destroyById(id, (err: Error) => {
      next(err);
    });
  }
}

export default function (Account: <Account>() => AccountInterface) {
  new AccountModel(Account);
};
