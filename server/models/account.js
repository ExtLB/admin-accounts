"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var _ = require("lodash");
var es6_promise_1 = require("es6-promise");
var loopback_1 = require("loopback");
var LoopBackSocket = require('loopback-socket');
var log = require('@dosarrest/loopback-component-logger')('server/model/Account');
var AccountInterface = (function (_super) {
    __extends(AccountInterface, _super);
    function AccountInterface() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AccountInterface;
}(loopback_1.PersistedModel));
exports.AccountInterface = AccountInterface;
exports.AccountType = {
    REGULAR: '$regularAccount',
    OWNER: '$ownerAccount',
    RESELLER: '$resellerAccount'
};
var Account = (function (_super) {
    __extends(Account, _super);
    function Account() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Account;
}(loopback_1.PersistedModel));
exports.Account = Account;
var AccountModel = (function () {
    function AccountModel(model) {
        var me = this;
        me.model = model;
        me.setup();
    }
    AccountModel.prototype.setup = function () {
        var me = this;
        return me.setupEvents().then(function (events) {
            return me.setupModel();
        }).then(function (methods) {
            return me.setupMethods();
        })["catch"](function (err) {
            log.error(err);
        });
    };
    AccountModel.prototype.setupModel = function () {
        var me = this;
        return new es6_promise_1.Promise(function (resolve, reject) {
            resolve(true);
        });
    };
    AccountModel.prototype.setupEvents = function () {
        var me = this;
        return new es6_promise_1.Promise(function (resolve, reject) {
            me.model.on('attached', function (app) {
                me.app = app;
                me.addResolvers();
                me.lbSocket = LoopBackSocket.get('app');
                log.info('Attached DISAccount Model to Application');
            });
            resolve(true);
        });
    };
    AccountModel.prototype.setupMethods = function () {
        var me = this;
        return new es6_promise_1.Promise(function (resolve, reject) {
            me.model.remoteMethod('getTree', {
                accepts: { arg: 'id', type: 'string', required: true },
                returns: { arg: 'results', type: 'object', root: true },
                http: { verb: 'get', path: '/tree/:id' }
            });
            me.model.getTree = me.getTree;
            me.model.remoteMethod('patchTree', {
                accepts: [
                    { arg: 'id', type: 'string', required: true },
                    { arg: 'name', type: 'string', required: false },
                    { arg: 'description', type: 'string', required: false },
                    { arg: 'parentId', type: 'string', required: false },
                    { arg: 'type', type: 'string', required: false },
                ],
                returns: { arg: 'results', type: 'object', root: true },
                http: { verb: 'patch', path: '/tree/:id' }
            });
            me.model.patchTree = me.patchTree;
            me.model.remoteMethod('deleteTree', {
                accepts: { arg: 'id', type: 'string', required: true },
                returns: { arg: 'results', type: 'object', root: true },
                http: { verb: 'delete', path: '/tree/:id' }
            });
            me.model.deleteTree = me.deleteTree;
            resolve(true);
        });
    };
    AccountModel.prototype.addResolvers = function () {
        var me = this;
        var app = me.app;
        var Role = app.registry.getModelByType('Role');
        var User = app.registry.getModelByType('User');
        var Account = app.registry.getModelByType('Account');
        Role.registerResolver(exports.AccountType.OWNER, function (role, context, cb) {
            if (context.modelName === 'Account') {
                return process.nextTick(function () { return cb(null, false); });
            }
            var userId = context.accessToken.userId;
            if (!userId) {
                return process.nextTick(function () { return cb(null, false); });
            }
            User.findById(userId, {
                include: ['account']
            }).then(function (user) {
                var account = user.account();
                switch (account.type) {
                    case exports.AccountType.OWNER:
                        cb(null, true);
                        break;
                    default:
                        cb(null, false);
                }
            })["catch"](function (err) {
                log.error(err);
                cb(err, false);
            });
        });
        Role.registerResolver(exports.AccountType.RESELLER, function (role, context, cb) {
            if (context.modelName === 'Account') {
                return process.nextTick(function () { return cb(null, false); });
            }
            var userId = context.accessToken.userId;
            if (!userId) {
                return process.nextTick(function () { return cb(null, false); });
            }
            User.findById(userId, {
                include: ['account']
            }).then(function (user) {
                var account = user.account();
                switch (account.type) {
                    case exports.AccountType.RESELLER:
                        cb(null, true);
                        break;
                    default:
                        cb(null, false);
                }
            })["catch"](function (err) {
                log.error(err);
                cb(err, false);
            });
        });
    };
    AccountModel.prototype.getTree = function (id, next) {
        this.find({ where: { parentId: (id === 'root' ? "" : id) }, counts: ['users'] }, function (err, results) {
            _.each(results, function (result, id) {
                results[id].iconCls = 'x-fa fa-users';
                if (result.type !== exports.AccountType.RESELLER) {
                    results[id].leaf = true;
                    results[id].iconCls = 'x-fa fa-user';
                }
            });
            next(err, { children: results, total: results.length, success: true });
        });
    };
    AccountModel.prototype.patchTree = function (id, name, description, parentId, type, next) {
        this.findById(id, function (err, account) {
            if (err) {
                next(err);
            }
            else {
                var changes = {};
                if (name) {
                    changes.name = name;
                }
                if (description) {
                    changes.description = description;
                }
                if (type) {
                    changes.type = type;
                }
                if (parentId) {
                    changes.parentId = parentId;
                }
                if (type === exports.AccountType.RESELLER || type === exports.AccountType.OWNER) {
                    changes.parentId = '';
                }
                account.updateAttributes(changes, function (updateErr, result) {
                    next(updateErr, result);
                });
            }
        });
    };
    AccountModel.prototype.deleteTree = function (id, next) {
        this.destroyById(id, function (err) {
            next(err);
        });
    };
    return AccountModel;
}());
function default_1(Account) {
    new AccountModel(Account);
}
exports["default"] = default_1;
;
//# sourceMappingURL=account.js.map