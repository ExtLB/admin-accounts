Ext.define('Client.view.admin.user.EditorController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.user.editor',

  onAddRoleMap: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let user = vm.get('user');
    let userRoles = undefined;
    if (user.get !== undefined) {
      userRoles = user.get('roles');
    }
    let roleStore = vm.get('roleStore');
    let roleMapStore = vm.get('roleMapStore');
    roleStore.clearFilter();
    if (userRoles !== undefined && userRoles.length > 0) {
      roleStore.filterBy((role) => {
        return _.isNull(roleMapStore.findRecord('id', role.get('id')));
      });
    }
    let roleSelector = Ext.create('Client.view.admin.accounts.RoleSelector', {
      bind: {
        title: '{"saa:ADD_ROLE":translate}'
      },
      handler: function(newRoles) {
        let newRoleMaps = newRoles.map((newRole) => {
          return {
            principalType: 'USER',
            principalId: user.get('id'),
            roleId: newRole.get('id'),
            role: {
              id: newRole.get('id'),
              name: newRole.get('name')
            }
          };
        });
        roleMapStore.add(newRoleMaps);
        roleMapStore.sync({
          success: function() {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_USER_ROLE_SUCCESS'));
          },
          failure: function() {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_USER_ROLE_FAILURE'));
          }
        });
      },
      roleStore: roleStore
    });
    roleSelector.getViewModel().set('apiUrl', vm.get('apiUrl'));
    roleSelector.show();
  },
  onRemoveRoleMap: function(owner, eOpts) {
    let rec = eOpts.record;
    let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_MESSAGE'));
    Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_TITLE'), template.apply({rec: rec}), (btn) => {
      if (btn === 'yes') {
        rec.erase({
          callback: (record, operation, success) => {
            if (success !== true) {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_ERROR'));
            } else {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_SUCCESS'));
            }
          }
        });
      }
    });
  },

  okHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (view.handler) {
      view.handler(vm.get('user'));
    }
    view.destroy();
  },
  cancelHandler: function(btn) {
    this.getView().destroy();
  },

  onInitialize: function (editor) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (!_.isUndefined(view.userStore)) {
      vm.set('userStore', view.userStore);
    }
    if (!_.isUndefined(view.user)) {
      vm.set('user', view.user);
      vm.set('action', 'edit');
      view.setBind({
        title: '{"saa:EDIT_USER":translate} {user.username}',
      });
      let mapParams = Ext.decode(vm.get('extraRoleMapParams.filter'), true);
      mapParams.where.principalId = view.user.get('id');
      vm.set('extraRoleMapParams', {filter: Ext.encode(mapParams)});
      let apiUrl = Ext.Viewport.down('perspective\\.admin').getViewModel().get('apiUrl');
      vm.set('apiUrl', apiUrl);
      setTimeout(() => {
        let roleMapStore = me.lookup('roleGrid').getStore();
        roleMapStore.load();
      }, 10);
    } else {
      vm.set('user', view.userStore.getModel().create({
        username: '',
        email: '',
        password: ''
      }));
    }
  }
});
