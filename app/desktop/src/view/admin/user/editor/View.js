Ext.define('Client.view.admin.user.Editor', {
  extend: 'Ext.Dialog',
  xtype: 'admin.user.editor',
  requires: [
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.grid.Grid',
    'Ext.form.Panel',
    'Ext.field.Select',
    'Ext.field.Email',
    'Ext.field.Password',
    'Ext.dataview.List'
  ],
  viewModel: {type: 'admin.user.editor'},
  controller: {type: 'admin.user.editor'},
  bind: {
    title: '{"mod-admin-accounts:ADD_USER":translate} {user.username}',
  },
  layout: 'fit',
  closable: true,
  height: 600,
  width: 500,
  listeners: {
    initialize: 'onInitialize'
  },
  items: [{
    xtype: 'formpanel',
    reference: 'accountUserForm',
    layout: 'vbox',
    padding: 10,
    items: [{
      xtype: 'textfield',
      name: 'username',
      required: true,
      bind: {
        label: '{"mod-admin-accounts:USER_USERNAME":translate}',
        value: '{user.username}'
      }
    }, {
      xtype: 'emailfield',
      name: 'email',
      required: true,
      bind: {
        label: '{"mod-admin-accounts:USER_EMAIL":translate}',
        value: '{user.email}'
      },
      validators: ['email']
    }, {
      xtype: 'passwordfield',
      name: 'password',
      bind: {
        label: '{"mod-admin-accounts:USER_PASSWORD":translate}',
        value: '{user.password}'
      }
    }, {
      xtype: 'grid',
      shadow: true,
      reference: 'roleGrid',
      flex: 1,
      hideHeaders: true,
      bind: {
        store: '{roleMapStore}',
        hidden: '{action==="add"}'
      },
      items: [{
        xtype: 'toolbar',
        docked: 'top',
        items: [{
          xtype: 'label',
          bind: {
            html: '{"ROLES":translate}',
          }
        }, {
          xtype: 'spacer'
        }, {
          xtype: 'button',
          ui: 'round raised',
          iconCls: 'x-fa fa-plus',
          handler: 'onAddRoleMap',
          bind: {
            tooltip: '{"ADD":translate}'
          }
        }]
      }],
      columns: [{
        xtype: 'gridcolumn',
        flex: 1,
        dataIndex: 'role',
        tpl: '{role.name}',
        cell: {
          tools: {
            remove: {
              iconCls: 'x-fa fa-remove',
              handler: 'onRemoveRoleMap',
              zone: 'end',
              bind: {
                tooltip: '{"mod-admin-accounts:REMOVE_ROLE_MAP":translate}'
              },
            }
          }
        }
      }]
    }]
  }, {
    xtype: 'toolbar',
    docked: 'bottom',
    items: [{
      xtype: 'button',
      ui: 'rounded raised',
      bind: {
        disabled: '{!user.dirty}'
      },
      text: 'Ok',
      handler: 'okHandler'
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'button',
      ui: 'rounded raised',
      bind: {
        disabled: '{!user.dirty}'
      },
      text: 'Cancel',
      handler: 'cancelHandler'
    }]
  }]
});
