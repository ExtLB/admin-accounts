Ext.define('Client.view.admin.user.EditorViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.admin.user.editor',

  data: {
    action: "add",
    apiUrl: '/api/v1',
    user: {},
    extraRoleMapParams: {
		  filter: '{"where": {"principalType":"USER"}, "include": [{"relation": "role", "scope": {"fields": ["name"]} }] }'
    }
  },
  stores: {
    roleMapStore: {
	    autoLoad: false,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        type: 'date',
        name: 'created'
      }, {
        type: 'date',
        name: 'modified'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/roleMappings',
        extraParams: '{extraRoleMapParams}',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PUT",
          destroy:"DELETE"
        },
        reader: {
          totalProperty: 'total',
          rootProperty: 'data',
          type: 'json'
        }
      }
    },
    roleStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        type: 'date',
        name: 'created'
      }, {
        type: 'date',
        name: 'modified'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/roles',
        reader: {
          totalProperty: 'total',
          rootProperty: 'data',
          type: 'json'
        }
      }
    }
  }
});
