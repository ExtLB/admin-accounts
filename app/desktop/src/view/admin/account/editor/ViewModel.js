Ext.define('Client.view.admin.account.EditorViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.admin.account.editor',

  data: {
    apiUrl: '/api/v1',
    action: "add",
    account: {},
    extraAccountParams: {
		  filter: '{"where": {"type":"$resellerAccount"}}'
    }
  },
  formulas: {
    resellerHidden: function (get) {
      let action = get('action');
      let accountType = get('account.type');
      if (action === 'add') {
        return accountType !== '$regularAccount';
      } else {
        return accountType !== '$regularAccount';
      }
    }
  },
  stores: {
    resellers: {
      autoLoad: true,
      fields: [{
        name: 'id',
        type: 'string',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        name: 'parentId'
      }, {
        name: 'type'
      }, {
        type: 'date',
        name: 'created'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Accounts',
        filterParam: 'filters',
        extraParams: '{extraAccountParams}',
        idParam: 'id',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json',
          totalProperty: 'total',
          rootProperty: 'data'
        }
      }
    }
  }
});
