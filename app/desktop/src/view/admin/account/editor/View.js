Ext.define('Client.view.admin.account.Editor', {
  extend: 'Ext.Dialog',
  xtype: 'admin.account.editor',
  requires: [
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.form.Panel',
    'Ext.field.Select',
    'Ext.field.ComboBox',
    'Ext.field.TextArea',
    'Ext.dataview.List'
  ],
  viewModel: {type: 'admin.account.editor'},
  controller: {type: 'admin.account.editor'},
  bind: {
    title: '{"mod-admin-accounts:ADD_ACCOUNT":translate} {account.name}',
  },
  layout: 'fit',
  closable: true,
  height: 500,
  width: 400,
  listeners: {
    initialize: 'onInitialize',
    destroy: 'onDestroy'
  },
  items: [{
    xtype: 'formpanel',
    reference: 'accountForm',
    layout: 'vbox',
    items: [{
      xtype: 'textfield',
      name: 'name',
      required: true,
      bind: {
        label: '{"mod-admin-accounts:ACCOUNT_NAME":translate}',
        value: '{account.name}',
        disabled: '{account.name==="Owner"}'
      }
    }, {
      xtype: 'selectfield',
      reference: 'selectTypeField',
      name: 'type',
      required: true,
      listeners: {
        change: 'onTypeChange'
      },
      bind: {
        label: '{"mod-admin-accounts:ACCOUNT_TYPE":translate}',
        value: '{account.type}',
        disabled: '{account.name==="Owner"}'
      },
      options: []
    }, {
      xtype: 'comboboxfield',
      reference: 'selectParentIdField',
      name: 'parentId',
      forceSelection: true,
      clearable: true,
      editable: false,
      queryMode: 'local',
      displayField: 'name',
      valueField: 'id',
      bind: {
        store: '{resellers}',
        label: '{"mod-admin-accounts:RESELLER":translate}',
        value: '{account.parentId}',
        hidden: '{account.type !== "$regularAccount"}'
      }
    }, {
      xtype: 'textareafield',
      name: 'description',
      required: true,
      flex: 1,
      bind: {
        label: '{"mod-admin-accounts:ACCOUNT_DESCRIPTION":translate}',
        value: '{account.description}'
      }
    }]
  }, {
    xtype: 'toolbar',
    docked: 'bottom',
    items: [{
      xtype: 'button',
      text: 'Ok',
      ui: 'rounded raised',
      handler: 'okHandler',
      bind: {
        disabled: '{!account.dirty}'
      }
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'button',
      text: 'Cancel',
      ui: 'rounded raised',
      handler: 'cancelHandler',
      bind: {
        disabled: '{!account.dirty}'
      }
    }]
  }]
});
