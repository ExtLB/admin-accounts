Ext.define('Client.view.admin.account.EditorController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.account.editor',

  okHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (view.handler) {
      view.handler(vm.get('account'));
    }
    view.destroy();
  },
  cancelHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    vm.get('account').reject();
    this.getView().destroy();
  },
  onDestroy: function() {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    vm.get('account').reject();
  },

  onTypeChange: function(field, newValue, oldValue) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let account = vm.get('account');
    if (newValue === '$regularAccount' && account.getModified('type') === '$regularAccount') {
      account.set('parentId', account.getModified('parentId'));
    } else if (account.isModified('type') && account.getModified('parentId') !== '') {
      account.set('parentId', '');
    }
  },

  onInitialize: function (editor) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    me.lookup('selectTypeField').setOptions([{
      text: i18next.t('ACCOUNT.TYPE.$regularAccount'),
      value: '$regularAccount'
    }, {
      text: i18next.t('ACCOUNT.TYPE.$ownerAccount'),
      value: '$ownerAccount'
    }, {
      text: i18next.t('ACCOUNT.TYPE.$resellerAccount'),
      value: '$resellerAccount'
    }]);
    if (!_.isUndefined(view.account)) {
      vm.set('account', view.account);
      vm.set('action', 'edit');
      view.setBind({
        title: '{"saa:EDIT_ACCOUNT":translate} {account.name}',
      });
    } else {
      vm.set('account', view.accountStore.getModel().create({
        name: '',
        description: '',
        type: '$regularAccount'
      }));
    }
  }
});
