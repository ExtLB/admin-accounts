Ext.define('Client.view.admin.accounts.RoleSelectorController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.accounts.roleselector',

  okHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let roleList = me.lookup('roleList');
    let recs = roleList.getSelectable().getSelectedRecords();
    if (view.handler) {
      view.handler(recs);
    }
    view.destroy();
  },
  cancelHandler: function(btn) {
    this.getView().destroy();
  },

  onRoleSelectorInitialize: function (roleSelector) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    vm.setStores({
      roleStore: view.roleStore
    });
  }
});
