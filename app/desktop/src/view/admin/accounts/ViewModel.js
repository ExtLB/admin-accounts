Ext.define('Client.view.admin.accounts.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.admin.accounts',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json',
    'Ext.data.TreeStore',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json',
    'Ext.data.field.Field'
  ],
	data: {
		selecting: true,
    account: {},
    extraUserParams: {
		  filter: '{"include": ["roles"]}'
    },
    extraAccountParams: {
		  filter: '{"counts": ["users"]}'
    }
	},
  stores: {
	  userStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'realm'
      }, {
        name: 'username'
      }, {
        name: 'email'
      }, {
        name: 'emailVerified'
      }, {
        name: 'password'
      }, {
        name: 'roles',
        persist: false
      }, {
        name: 'accountId'
      }, {
        name: 'account'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Accounts/{account.id}/users',
        filterParam: 'filters',
        extraParams: '{extraUserParams}',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PUT",
          destroy:"DELETE"
        },
        reader: {
          type: 'json',
          totalProperty: 'total',
          rootProperty: 'data'
        }
      }
    },
	  accountStore: {
	    autoLoad: false,
      fields: [{
        name: 'id',
        type: 'string',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        name: 'parentId'
      }, {
        name: 'type'
      }, {
        type: 'date',
        name: 'created'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Accounts',
        filterParam: 'filters',
        extraParams: '{extraAccountParams}',
        idParam: 'id',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json',
          totalProperty: 'total',
          rootProperty: 'data'
        }
      }
    },
    accountTreeStore: {
	    type: 'tree',
	    autoLoad: true,
      rootVisible: false,
      root: {
	      expanded: true,
      },
      fields: [{
        name: 'id',
        type: 'string',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        name: 'parentId'
      }, {
        name: 'type'
      }, {
        type: 'date',
        name: 'created'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Accounts/tree',
        filterParam: 'filters',
        extraParams: '{extraAccountParams}',
        idParam: 'id',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json'
        }
      }
    }
  }
});
