Ext.define('Client.view.admin.accounts.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.admin.accounts',

  onSelectAccount: function(dataview, selected, eOpts) {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
    vm.set('selecting', true);
	  vm.set('account', selected);
    setTimeout(() => {
      vm.getStore('userStore').load({
        callback: function() {
          me.lookup('accountUserGrid').setMasked(false);
        }
      });
    }, 10);
  },
  onDeselectAccount: function(dataview, selected, eOpts) {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let agGrid = me.lookup('accountUserGrid');
    agGrid.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT")
    });
  },

  removeAccountHandler: function(owner, tool, evt) {
    let vm = owner.parentMenu.ownerCmp.getViewModel();
    let rec = vm.get('account');
    // console.log(owner, tool, evt);
    let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_MESSAGE'));
	  Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_TITLE'), template.apply({rec: rec}), (btn) => {
	    if (btn === 'yes') {
	      rec.erase({
          callback: (record, operation, success) => {
            if (success !== true) {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_ERROR'));
            } else {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_SUCCESS'));
            }
          }
        });
      }
    });
  },

  onSendAccountUserEmail: function(owner, tool, evt) {
    let rec = owner.getRecord();
  },
  onRemoveAccountUser: function(owner, tool, evt) {
    let vm = owner.parentMenu.ownerCmp.getViewModel();
    let rec = vm.get('accountUser');
    if (rec.get('id') === Client.app.getController('Authentication').user.id) {
     Ext.Msg.alert(i18next.t('saa:ERROR'), i18next.t('saa:ERROR_REMOVE_ACCOUNT_USER_MINE'));
    } else {
      let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_MESSAGE'));
      Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_TITLE'), template.apply({rec: rec}), (btn) => {
        if (btn === 'yes') {
          rec.erase({
            callback: (record, operation, success) => {
              if (success !== true) {
                Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ERROR'));
              } else {
                Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_SUCCESS'));
              }
            }
          });
        }
      });
    }
  },

  onAddRoleMap: function(btn) {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let user = me.lookup('accountUserGrid').getSelectable().getSelectedRecord();
    let userRoles = user.get('roles');
	  let roleStore = vm.get('roleStore');
    let roleMapStore = vm.get('roleMapStore');
	  roleStore.clearFilter();
	  if (userRoles.length > 0) {
      roleStore.filterBy((role) => {
        return _.isNull(roleMapStore.findRecord('id', role.get('id')));
      });
    }
	  let roleSelector = Ext.create('Client.view.admin.accounts.RoleSelector', {
	    bind: {
	      title: '{"saa:ADD_ROLE":translate}'
      },
      handler: function(newRoles) {
	      let newRoleMaps = newRoles.map((newRole) => {
	        return {
	          principalType: 'USER',
            principalId: user.get('id'),
            roleId: newRole.get('id'),
            role: {
	            id: newRole.get('id'),
              name: newRole.get('name')
            }
          };
        });
	      roleMapStore.add(newRoleMaps);
	      roleMapStore.sync({
          success: function() {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_USER_ROLE_SUCCESS'));
          },
          failure: function() {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_USER_ROLE_FAILURE'));
          }
        });
      },
      roleStore: roleStore
    });
	  roleSelector.getViewModel().set('apiUrl', vm.get('apiUrl'));
	  roleSelector.show();
  },
  onRemoveRoleMap: function(owner, eOpts) {
    let rec = eOpts.record;
    let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_MESSAGE'));
    Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_TITLE'), template.apply({rec: rec}), (btn) => {
      if (btn === 'yes') {
        rec.erase({
          callback: (record, operation, success) => {
            if (success !== true) {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_ERROR'));
            } else {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ROLE_SUCCESS'));
            }
          }
        });
      }
    });
  },

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	},
  onAddAccount: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let store = vm.getStore('accountStore');
    let treeStore = vm.getStore('accountTreeStore');
	  let editor = Ext.create('Client.view.admin.account.Editor', {
	    handler: function(account) {
	      store.add(account);
	      store.sync({
          success: () => {
            treeStore.load();
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_FAILURE'));
          }
        });
      },
      accountStore: treeStore
    });
	  editor.show();
  },
  onRefreshAccounts: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  vm.getStore('accountTreeStore').load();
  },

  onAddAccountUser: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let store = vm.getStore('userStore');
	  let editor = Ext.create('Client.view.admin.user.Editor', {
	    handler: function(user) {
	      store.add(user);
	      store.sync({
          success: () => {
            store.load();
            Ext.toast(i18next.t('saa:ADD_USER_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:ADD_USER_FAILURE'));
          }
        });
      },
      userStore: store
    });
	  editor.show();
  },

  onAccountListInitialize: function(list) {
	  let me = this;
	  let view = me.getView();
    list.el.on({
      scope: me,
      contextmenu: me.onAccountContextMenu
    });
    Ext.Object.each(Client.tool.admin.account, (tool) => {
      view.accountMenu.items.push(Client.tool.admin.account[tool].config);
    });
    view.accountMenu.items.push({
      separator: true,
      handler: 'removeAccountHandler',
      bind: {
        text: '{"mod-admin-accounts:REMOVE_ACCOUNT":translate}',
        hidden: '{accountData.name==="Owner"}'
      },
      margin: '10 0 0',
      iconCls: 'x-fa fa-times'
    });
    if (view.accountMenu.items.length > 2) {
      view.accountMenu.items[1].separator = true;
    }
  },
  onAccountUserGridInitialize: function(grid) {
	  let me = this;
	  let view = me.getView();
	  // let agGrid = me.lookup('accountUserGrid');
    grid.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT")
    });
    grid.el.on({
      scope: me,
      contextmenu: me.onUserContextMenu
    });
    Ext.Object.each(Client.tool.admin.user, (tool) => {
      view.userMenu.items.push(Client.tool.admin.user[tool].config);
    });
    view.userMenu.items.push({
      separator: true,
      handler: 'onRemoveAccountUser',
      bind: {
        text: '{"mod-admin-accounts:REMOVE_ACCOUNT_USER":translate}'
      },
      margin: '10 0 0',
      iconCls: 'x-fa fa-times'
    });
    if (view.userMenu.items.length > 2) {
      view.userMenu.items[1].separator = true;
    }
  },
  destroy: function() {
	  this.userToolMenu = Ext.destroy(this.userToolMenu);
    this.accountToolMenu = Ext.destroy(this.accountToolMenu);
	  this.callParent();
  },

  onUserMenu: function (grid, context) {
    this.updateUserMenu(context.record, context.tool.el, context.event, 'r-l?');
  },
  updateUserMenu: function (record, el, e, align) {
    let menu = this.getUserMenu();
    this.getViewModel().set('accountUser', record);
    this.getViewModel().set('accountUserData', record.getData());
    menu.autoFocus = !e.pointerType;
    menu.showBy(el, align);
  },
  getUserMenu: function () {
    let menu = this.userToolMenu,
      view = this.getView();

    if (!menu) {
      this.userToolMenu = menu = Ext.create(Ext.apply({
        ownerCmp: view
      }, view.userMenu));
    }

    return menu;
  },
  onUserContextMenu: function (e) {
    let grid = this.lookup('accountUserGrid'),
      target = e.getTarget(grid.itemSelector),
      item;

    if (target) {
      e.stopEvent();

      item = Ext.getCmp(target.id);
      if (item) {
        this.updateUserMenu(item.getRecord(), item.el, e, 't-b?');
      }
    }
  },

  onAccountMenu: function (grid, context) {
    this.updateAccountMenu(context.record, context.tool.el, context.event, 'r-l?');
  },
  updateAccountMenu: function (record, el, e, align) {
    let menu = this.getAccountMenu();
    this.getViewModel().set('account', record);
    this.getViewModel().set('accountData', record.getData());
    menu.autoFocus = !e.pointerType;
    menu.showBy(el, align);
  },
  getAccountMenu: function () {
    let menu = this.accountToolMenu,
      view = this.getView();

    if (!menu) {
      this.accountToolMenu = menu = Ext.create(Ext.apply({
        ownerCmp: view
      }, view.accountMenu));
    }

    return menu;
  },
  onAccountContextMenu: function (e) {
    let list = this.lookup('accountList'),
      target = e.getTarget(list.itemSelector),
      item;

    if (target) {
      e.stopEvent();

      item = Ext.getCmp(target.id);
      if (item) {
        this.updateAccountMenu(item.getRecord(), item.el, e, 't-b?');
      }
    }
  }
});
