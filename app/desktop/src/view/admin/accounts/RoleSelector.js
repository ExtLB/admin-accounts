Ext.define('Client.view.admin.accounts.RoleSelector', {
  extend: 'Ext.Dialog',
  xtype: 'admin.accounts.roleselector',
  requires: [
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.dataview.List'
  ],
  viewModel: {type: 'admin.accounts.roleselector'},
  controller: {type: 'admin.accounts.roleselector'},
  title: 'Role Selector',
  layout: 'fit',
  closable: true,
  height: 500,
  width: 300,
  listeners: {
    initialize: 'onRoleSelectorInitialize'
  },
  items: [{
    xtype: 'list',
    reference: 'roleList',
    bind: {
      store: '{roleStore}'
    },
    selectable: {
      mode: 'multi'
    },
    itemTpl: [
      '<div>{name}</div>'
    ]
  }, {
    xtype: 'toolbar',
    docked: 'bottom',
    items: [{
      xtype: 'button',
      text: 'Ok',
      handler: 'okHandler'
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'button',
      text: 'Cancel',
      handler: 'cancelHandler'
    }]
  }]
});
