Ext.define('Client.view.admin.accounts.RoleSelectorViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.admin.accounts.roleselector',

  stores: {
    roleStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        type: 'date',
        name: 'created'
      }, {
        type: 'date',
        name: 'modified'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/roles',
        reader: {
          totalProperty: 'total',
          rootProperty: 'data',
          type: 'json'
        }
      }
    }
  }
});
