Ext.define('Client.view.admin.accounts.View', {
	// extend: 'Ext.grid.Grid',
  extend: 'Ext.Container',
	xtype: 'admin.accounts',
	cls: 'admin-accounts',
	requires: [
	  'Ext.XTemplate',
    'Ext.layout.Form',
    'Ext.layout.HBox',
    'Ext.layout.VBox',
    'Ext.dataview.List',
    'Ext.grid.Grid',
    'Ext.grid.Tree',
    'Ext.grid.column.Tree',
    'Ext.panel.Resizer',
    'Ext.form.Panel',
    'Ext.field.ComboBox',
    'Ext.field.Email',
    'Ext.field.Password',
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.Toast',
    'Ext.LoadMask',
    'Ext.data.validator.Email',
    'Client.view.admin.accounts.RoleSelector',
    'Client.view.admin.account.Editor',
    'Client.tool.admin.account.*',
    'Client.tool.admin.user.*'
  ],
	controller: {type: 'admin.accounts'},
	viewModel: {type: 'admin.accounts'},
  perspectives: ['admin'],

  layout: 'hbox',
  items: [{
    xtype: 'container',
    layout: 'vbox',
    width: 300,
    items: [{
      reference: 'accountList',
      xtype: 'tree',
      width: 300,
      height: '100%',
      bind: {
        store: '{accountTreeStore}'
      },
      listeners: {
        initialize: 'onAccountListInitialize',
        select: 'onSelectAccount',
        deselect: 'onDeselectAccount'
      },
      selectable: {mode: 'single'},
      columns: [
        {
          xtype: 'treecolumn',
          flex: '1',
          dataIndex: 'name',
          text: 'Name',
          cell: {
            tools: {
              menu: {
                handler: 'onAccountMenu',
                zone: 'end',
                bind: {
                  tooltip: '{"mod-admin-accounts:USER_OPTIONS":translate}'
                },
                iconCls: 'x-fa fa-bars'
              }
            }
          }
        }
      ]
    }, {
      xtype: 'toolbar',
      docked: 'top',
      items: [{
        xtype: 'button',
        ui: 'round raised',
        handler: 'onRefreshAccounts',
        iconCls: 'x-fa fa-sync'
      }, {
        xtype: 'spacer'
      }, {
        xtype: 'button',
        ui: 'round raised',
        handler: 'onAddAccount',
        iconCls: 'x-fa fa-plus'
      }]
    }]
  }, {
    xtype: 'container',
    layout: 'vbox',
    flex: 1,
    items: [{
      xtype: 'grid',
      flex: 1,
      reference: 'accountUserGrid',
      bind: {
        store: '{userStore}'
      },
      masked: {
        xtype: 'loadmask'
      },
      listeners: {
        initialize: 'onAccountUserGridInitialize'
      },
      items: [{
        xtype: 'toolbar',
        docked: 'top',
        items: [{
          xtype: 'spacer'
        }, {
          xtype: 'button',
          ui: 'round raised',
          handler: 'onAddAccountUser',
          iconCls: 'x-fa fa-plus'
        }]
      }],
      columns: [{
        bind: {
          text: '{"mod-admin-accounts:USER_COLUMN.USERNAME":translate}',
        },
        dataIndex: 'username',
        flex: 1,
        cell: {userCls: 'bold'}
      }, {
        bind: {
          text: '{"mod-admin-accounts:USER_COLUMN.EMAIL":translate}',
        },
        dataIndex: 'email',
        flex: 2,
        cell: {
          tools: {
            sendEmail: {
              handler: 'onSendAccountUserEmail',
              zone: 'start',
              bind: {
                tooltip: '{"mod-admin-accounts:SEND_EMAIL":translate}'
              },
              iconCls: 'x-fa fa-envelope'
            },
            menu: {
              handler: 'onUserMenu',
              zone: 'end',
              bind: {
                tooltip: '{"mod-admin-accounts:USER_OPTIONS":translate}'
              },
              iconCls: 'x-fa fa-bars'
            }
          }
        }
      }]
    }]
  }],

  accountMenu: { // used by Controller
    xtype: 'menu',
    anchor: true,
    padding: 10,
    minWidth: 300,
    viewModel: {},
    items: [{
      xtype: 'component',
      indented: false,
      bind: {
        data: '{accountData}'
      },
      tpl: ['<div style="width: 75px;">' +
        '<div style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{name}</div>' +
        '<div style="color: grey; width: 200px;">' +
        '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Type: {[this.accountType(values.type)]}</div>' +
        '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Description: {description}</div>' +
        '</div>' +
        '</div>',
        {
          accountType: function(type) {
            return i18next.t(`ACCOUNT.TYPE.${type}`);
          }
        }]
    }]
  },

  userMenu: { // used by Controller
    xtype: 'menu',
    anchor: true,
    padding: 10,
    minWidth: 300,
    viewModel: {},
    items: [{
      xtype: 'component',
      indented: false,
      bind: {
        data: '{accountUserData}'
      },
      tpl: '<div style="background-image: url({photo}); background-repeat: no-repeat; width: 75px; padding-left: 90px;">' +
      '<div style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{username}</div>' +
      '<div style="color: grey; width: 200px;">' +
      '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Email: {email}</div>' +
      '<tpl if="roles.length !== 0">' +
      '<div>Roles:<br><ul>' +
      '<tpl for="roles"><li>{name}</li></tpl>' +
      '</ul></div>' +
      '</tpl>' +
      '</div>'
    }]
  }
  // bind: {
  //   store: '{accountStore}',
  // },
  // selectable: { mode: 'single' },
  // listeners: {
		// select: 'onItemSelected'
  // },

});
