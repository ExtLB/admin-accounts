Ext.define('Client.tool.admin.account.Edit', {
  extend: 'Ext.Tool',
  type: 'admin-account-tool.edit',

});
Client.tool.admin.account.Edit.addStatics({
  config: {
    handler: function (owner, tool, evt) {
      let vm = owner.parentMenu.ownerCmp.getViewModel();
      let editor = Ext.create('Client.view.admin.account.Editor', {
        handler: function(account) {
          account.save({
            success: () => {
              Ext.toast(i18next.t('saa:SAVE_ACCOUNT_SUCCESS'));
            },
            failure: () => {
              Ext.toast(i18next.t('saa:SAVE_ACCOUNT_FAILURE'));
            }
          });
        },
        account: vm.get('account'),
        accountStore: vm.getStore('accountTreeStore')
      });
      editor.show();
    },
    bind: {
      text: '{"mod-admin-accounts:EDIT_ACCOUNT":translate}'
    },
    separator: true,
    margin: '10 0 0',
    iconCls: 'x-fa fa-edit'
  }
});
