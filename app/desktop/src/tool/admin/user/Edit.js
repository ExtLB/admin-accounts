Ext.define('Client.tool.admin.user.Edit', {
  extend: 'Ext.Tool',
  type: 'admin-user-tool.edit',

});
Client.tool.admin.user.Edit.addStatics({
  config: {
    handler: function (owner, tool, evt) {
      let vm = owner.parentMenu.ownerCmp.getViewModel();
      let store = vm.getStore('userStore');
      let editor = Ext.create('Client.view.admin.user.Editor', {
        handler: function(user) {
          user.save({
            success: () => {
              Ext.toast(i18next.t('saa:SAVE_ACCOUNT_SUCCESS'));
            },
            failure: () => {
              Ext.toast(i18next.t('saa:SAVE_ACCOUNT_FAILURE'));
            }
          });
        },
        user: vm.get('accountUser'),
        userStore: store
      });
      editor.show();
    },
    bind: {
      text: '{"mod-admin-accounts:EDIT_ACCOUNT_USER":translate}'
    },
    separator: true,
    margin: '10 0 0',
    iconCls: 'x-fa fa-edit'
  }
});
